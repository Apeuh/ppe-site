<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Menu</title>
  <link rel="stylesheet" href="./css/login.css">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/QSN.css">
  <?php require_once('php/db_connect.php');?>
</head>
<?php include_once('header.php');?>
<div id='QSN'>
    <h1> Qui Sommes-nous? </h1><br>
    <section>
        <p>Nous sommes deux étudiants en BTS SIO :<ul>
            <li>Alexandre Gomez 27 ans</li>
            <li>Camille Favareille 26 ans</li>
            <ul>
        </p>
    </section><br>
    <section>
        <h2 class=titre>Objectif</h2>
        <p class=texte>Nous avons réalisés cette application web dans l'optique d'un projet (PPE) durant notre Cursus de BTS.<br>
            Nous nous sommes répartis les tâches de façon à avoir une méthode le plus fidèle possible à un vrai projet en entreprise.<br>
            Tout d'abord Alexandre c'est occupé du Front-End de la globalité du site.
            Ensuite Camille à gérer le coté Back-End de la majorité du site.
        </p>
    </section><br>
    <section>
        <h2 class=titre>Front-End</h2>
        <p class=texte>Pour ce faire nous avons choisi de rendre le site responsive de divers façons.<br>
        Le bandeau lui est responsive grace à bootstrap.
        </p>
    </section><br>
    <section>
        <h2 class=titre>Back-End</h2>
        <p class=texte>Pour ce faire, nous avons utilisés la méthode Merise pour la base de donnée.<br>
            Pour compléter cela nous avons utilisés la méthode UML pour tout ce qui est classe.<br>
            La méthode MVC n'a pas été utilisé car nous n'avions pas encore vu cela.<br>
            Mais si cela était à refaire nous séparerons en deux les Controllers pour faire d'un coté les Controllers et de l'autre des Modèles qui s'occupent de la récuparation des données ainsi que les changement pour la BDD. 
        </p>
    </section>
</div>
   

<?php include_once('footer.php');
