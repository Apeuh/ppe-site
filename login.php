<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/login.css">
  <?php require_once('php/db_connect.php');?>
</head>
<?php 
include_once('header.php');
    if((empty($_SESSION['user']) || $_SESSION['user']=="" )&&(isset($_POST['log'])) ){
        echo '<script language="javascript">';
        echo 'alert("Mot de passe ou Mail Incorrect")';
        echo '</script>';
    }
    if(isset($_SESSION['user'])){
        if($_SESSION['user']->getRole()=="Cuisinier"){
            header("Location:cuisine.php");
        }
        else header("Location:compte.php");
    }

  ?>
<!--Login-->
<div id=mainForm>
    <div id=loginbox>
        <h3> Se Connecter </h3>
        <p>Connectez-vous à votre espace client <br> si vous avez déjà
    un compte sur notre site !</p>
        <form id='logform' method="post" action="login.php">
            <label for="logmail">E-mail :</label>
            <input id="logmail" name="logmail" type=text required placeholder="E-mail">
            <label for="logpass">Mot de Passe :</label>
            <input id="logpass" name="logpass" type=password required placeholder="Mot de passe"><br>
            <input id='log' name="log" type=submit value="Je me connecte" >
            <a href="forgot.php">Mot de passe oublié</a>
        </form>
    </div>
    <div id="blackspace">
        <br>
    </div>
    <!-- Inscription -->
    <div id=registerbox>
        <h3> S'inscrire </h3>
        <p>Si vous n'avez pas de compte sur notre site,<br>
    inscrivez-vous via ce formulaire !</p>
        <form id="registerform" method="post" action="login.php">
            <div class="flexend">
                <label for='name'>Nom :</label>
                <input id='name' name='name' type=text required placeholder="Nom">
                <label for='fname'>Prénom :</label>
                <input id='fname' name='fname' type=text required placeholder="Prénom">
                <label for='phone'>Téléphone :</label>
                <input id='phone' name='phone' type=text required placeholder="Téléphone">
                <label for='mail'>E-mail :</label>
                <input id='mail' name='mail' type=text onkeyup='regex("^[a-z0-9\.]+@[a-z]+\.[a-z]{2,3}$","mail")' required placeholder="E-mail">
                <label for='bdate'>Date de naissance :</label>
                <input id='bdate' name='bdate' type=date required placeholder="">
                <label for='password'>Mot de Passe :</label>
                <input id='password' name='password' onkeyup='regex("/^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[-+!*$@%_])([-+!*$@%_\w]{8,})$/gm","password")' type=password required placeholder="Mot de passe">
            </div>
            <div class="flexend">
                <label for='num_street'>Numéro de Rue :</label>
                <input id='num_street' name='num_street' type=number required placeholder="Numéro de Rue">
                <label for='street'>Rue :</label>
                <input id='street' name='street' type=text required placeholder="Nom de Rue">
                <label for='postal'>Code Postal :</label>
                <input id='postal' name='postal' type=text required placeholder="Code Postal">
                <label for='city'>Ville :</label>
                <input id='city' name='city' type=text required placeholder="Ville">
                <label for='country'>Pays :</label>
                <input id='country' name='country' type=country required placeholder="Pays">
                <br>
                <input id='register' name='register' type=submit value="Je m'inscrire">
            </div>
        </form>
        <b><p class='text-danger'>En cliquant sur le bouton ci-dessus,<br> vous acceptez que vos données soient utilisées<br> dans le cadre de l'utilisation du site Tortiplat.com</p><b>
        <p id='regmail'></p>                <p id='regpassword'></p>
        
    </div>
</div>
<?php 
echo"<script>";    
require_once('js/regex.js');
echo"</script>";
include_once('footer.php');
?>