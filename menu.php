<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Menu</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/menu.css">
  <?php require_once('./php/db_connect.php');
  ?>
</head> 
<?php include_once('header.php');
  include(__DIR__."/php/controller/ControllerMenu.php");?>

  <!-- Script pour le + et le - -->
  <script>
function plus(val){
  selector="#"+val+" .nombreArt";
  input = document.querySelector(selector);
  input.value++;}
function moins(val){
  selector="#"+val+" .nombreArt";
  input = document.querySelector(selector);
  if(input.value>1) input.value--;}

//Script pour l'ajout au panier
function addPanier(val) {
  var xhttp = new XMLHttpRequest();
  selector="#"+val+" .nombreArt";
  quantity=document.querySelector(selector).value;
  url="php/fonctions/addPanier.php?q="+quantity+"&p="+val;
  xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              alert(xhttp.responseText);
              ;};}
  xhttp.open("GET", url,true);
  xhttp.send();
}
</script>

<div class='flexcenter' id='tagchoice'>
        <a href='./menu.php?cat=all' >
          <input type="radio" id="all" name="menu" value="all" onclick="document.location.href='./menu.php?cat=all'"<?php if((isset($_GET['cat'])) && ($_GET['cat'] == 'all')) { echo 'checked'; } ?>>
          <label for="all">Tout</label>
        </a>
        <a href='./menu.php?cat=Menu'>
          <input type="radio" id="Menu" name="menu" value="Menu"onclick="document.location.href='./menu.php?cat=Menu'"<?php if((isset($_GET['cat'])) && ($_GET['cat'] == 'Menu')) { echo 'checked'; } ?> >
          <label for="Menu">Menu</label>
        </a>
        <a href='./menu.php?cat=Entrée'>
          <input type="radio" id="entrée" name="menu" value="Entrée"onclick="document.location.href='./menu.php?cat=Entrée'"<?php if((isset($_GET['cat'])) && ($_GET['cat'] == 'Entrée')) { echo 'checked'; } ?> >
          <label for="entrée">Entrée</label>
        </a>
        <a href='./menu.php?cat=Plat'>
          <input type="radio" id="plat" name="menu" value="Plat"onclick="document.location.href='./menu.php?cat=Plat'"<?php if((isset($_GET['cat'])) && ($_GET['cat'] == 'Plat')) { echo 'checked'; } ?> >
          <label for="plat">Plat</label>
        </a>
        <a href='./menu.php?cat=Dessert'>
          <input type="radio" id="dessert" name="menu" value="Dessert" onclick="document.location.href='./menu.php?cat=Dessert'" <?php if((isset($_GET["cat"])) && ($_GET["cat"] == "Dessert")) { echo "checked"; } ?> >
          <label for="dessert">Dessert</label>
        </a>
        <a href="./menu.php?cat=Boisson">
          <input type="radio" id="drink" name="menu" value="Boisson" onclick="document.location.href='./menu.php?cat=Boisson'"<?php if((isset($_GET['cat'])) && ($_GET['cat'] == 'Boisson')) { echo 'checked'; } ?>>
          <label for="drink">Boissons</label>
        </a>
</div>
<div id="mainMenu">
    <?php
        $ControllerArticle= new ControllerArticle; 
        $ControllerMenu = new ControllerMenu;
        $ControllerArticle->CreateArticle(); 
        $ControllerMenu->CreateMenu();
        //Création du Menu dans la SuperGlobale SESSION//
        if (isset($_POST["idMenu"])){
          $key='Menu|';
          foreach($_POST as $label){
            $key.= substr($label,0)."|";
          }
          $key=substr($key,0,-1);
          $quantity=1;
          if (isset($_SESSION["Panier"][$key])){
          $_SESSION["Panier"][$key]+=$quantity;
          }
          else {
            $_SESSION["Panier"][$key]=$quantity;
          }
        }
        // TRI PAR CATEGORIE (Menu) //
        if ($_GET['cat']=='Menu'||$_GET['cat']=='all'){
          // GENERATION DES CARTES -MENU //
          foreach($ControllerMenu -> getListe() as $Menu){
            echo '<form method="post" action="menu.php?cat=all">
                <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src= "./assets/plat_menu/Menu.jpg" alt="Card image cap">
                  <div class="card-body flexcard">
                    <label for="idMenu">
                    <input type="hidden" name="idMenu" value=',$Menu->getId(),'>
                    <h5 class="card-title">',$Menu->getName(),'</h5>
                    <div>
                      <div class="selectMenu">';
                        // DIFFERENTS SELECT EN FONCTION DU MENU //
                        echo '<select name="'.'listeMenuE'.$Menu->getId().'" id="'.'listeMenuE'.$Menu->getId().'">';
                        foreach ($Menu->getListeArt() as $idArt){
                          foreach ($ControllerArticle->getListe() as $article){
                            if ($article->getId()==$idArt && $article->getType()=='Entrée'){
                                echo "<option>".$article->getName()."</option>";
                              }
                          }
                        }
                        echo "</select>";
                        echo '<select name="'.'listeMenuP'.$Menu->getId().'" id="'.'listeMenuP'.$Menu->getId().'">';
                        foreach ($Menu->getListeArt() as $idArt){
                          foreach ($ControllerArticle->getListe() as $article){
                            if ($article->getId()==$idArt && $article->getType()=='Plat'){
                                echo "<option>".$article->getName()."</option>";
                            }
                          }
                        }
                        echo "</select>";
                        echo '<select name="'.'listeMenuD'.$Menu->getId().'" id="'.'listeMenuD'.$Menu->getId().'">';
                        foreach ($Menu->getListeArt() as $idArt){
                          foreach ($ControllerArticle->getListe() as $article){
                            if ($article->getId()==$idArt && $article->getType()=='Dessert'){
                                echo "<option>".$article->getName()."</option>";
                            }
                          }
                        }
                        echo "</select>";
                        echo '<select name="'.'listeMenuB'.$Menu->getId().'" id="'.'listeMenuB'.$Menu->getId().'">';
                        foreach ($Menu->getListeArt() as $idArt){
                          foreach ($ControllerArticle->getListe() as $article){
                            if ($article->getId()==$idArt && $article->getType()=='Boisson'){
                                echo "<option>".$article->getName()."</option>";
                            }
                          }
                        }
                        echo '</select>
                      </div>
                      <p name="price" class="card-text pricetag">', $Menu->getPrice(),' €</p>
                      <div class="box" id="article',$Menu->getId(),'">
                          <div class="buttonContainer">
                            <input type="submit"value="Ajouter au panier" class="btn btn-primary"></input>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>';
          }
        }
        // TRI PAR CATEGORIE (Article) //
        foreach($ControllerArticle -> getListe() as $article){
            if ($article->getType()==$_GET['cat']){
                        echo '<div class="card" style="width: 18rem;">
                                    <img class="card-img-top" src= "',$article->getImage(),'" alt="Card image cap">
                                    <div class="card-body flexcard">
                                        <h5 class="card-title">',$article->getName(),'</h5>
                                        <p class="card-text">', $article->getDescription(),'</p>
                                        <div>
                                          <p class="card-text pricetag">', $article->getPrice(),' €</p>
                                          <div class="box" id="article',$article->getId(),'">
                                              <div class="quantity">
                                                  <img onclick="moins(&quot;article'.$article->getId().'&quot;)" src="./assets/minus.png">
                                                  <input class="nombreArt" type="number" size="1" value="1">
                                                  <img onclick="plus(&quot;article'.$article->getId().'&quot;)" src="./assets/plus.png">
                                              </div>
                                              <div class="buttonContainer">
                                              <button onclick="addPanier(&quot;article'.$article->getId().'&quot;)" class="btn btn-primary">Ajouter au panier</button>
                                              </div>
                                          </div>
                                        </div>
                                    </div>
                                </div>';
            }

            // AFFICHAGE TOUT TYPE ARTICLE //
            else if ($_GET['cat']=='all'){
                echo '<div class="card" style="width: 18rem;">
                                    <img class="card-img-top" src= "',$article->getImage(),'" alt="Card image cap">
                                    <div class="card-body flexcard">
                                        <h5 class="card-title">',$article->getName(),'</h5>
                                        <p class="card-text">', $article->getDescription(),'</p>
                                        <div>
                                          <p class="card-text pricetag">', $article->getPrice(),' €</p>
                                            <div class="box" id="article',$article->getId(),'">
                                                <div class="quantity">
                                                    <img onclick="moins(&quot;article'.$article->getId().'&quot;)" src="./assets/minus.png">
                                                    <input class="nombreArt" type="number" size="1" value="1">
                                                    <img onclick="plus(&quot;article'.$article->getId().'&quot;)" src="./assets/plus.png">
                                                </div>
                                                <div class="buttonContainer">
                                                <button onclick="addPanier(&quot;article'.$article->getId().'&quot;)" class="btn btn-primary">Ajouter au panier</button>
                                                </div>
                                            </div>
                                          </div>
                                    </div>
                                </div>';
            }
        }
    ?> 
</div> 
<script>
// SCRIPT MASQUER SELECT VIDE DES MENUS GENERES //
let E = 'listeMenuE';
let P = 'listeMenuP';
let D = 'listeMenuD';
let B = 'listeMenuB';
for (let i=1;i<100;i++){
  var tempE=document.getElementById(E+i);
  var tempP=document.getElementById(P+i);
  var tempD=document.getElementById(D+i);
  var tempB=document.getElementById(B+i);
  if(tempE.value===''){
    tempE.style.display = 'none';
  }
  if(tempP.value===''){
      tempP.style.display = 'none';
  }
  if(tempD.value===''){
    tempD.style.display = 'none';
  }
  if(tempB.value===''){
    tempB.style.display = 'none';
  }
}
</script>
</div>
<?php include_once('footer.php'); ?>