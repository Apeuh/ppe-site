<?php
require_once( __DIR__ ."/../db_connect.php");
require( __DIR__ ."/../class/Adress.php");
class ControllerAdress{
        
    public $listeAdresse=array();

        function AddAdress($idAdresse){
            $dbconnect=new db_connect;
            $req=($dbconnect->connect())->prepare("SELECT * FROM address where id_address=?");
            $req->execute([$idAdresse]);
            while($data = $req->fetch(PDO::FETCH_ASSOC)){
                $_SESSION["adresse"]=new Adress($data["id_address"],$data["street"],$data["num_street"],$data["city"],$data["postal_code"],$data["country"]);
                $data["id_address"]=new Adress($data["id_address"],$data["street"],$data["num_street"],$data["city"],$data["postal_code"],$data["country"]);
                array_push($this->listeAdresse,$data["id_address"]);
            }
        }

        function AddAddresse($val1,$val2,$val3,$val4,$val5){
            $dbconnect=new db_connect;
            $req=$dbconnect->select("INSERT INTO `address`(`num_street`, `street`, `postal_code`, `city`, `country`) VALUES (?,?,?,?,?)");
              $req->execute([$val1,$val2,$val3,$val4,$val5]);
        }

        function checkAdress(Adress $adress,$para,$val){
            switch($para) {
                case "NStreet" :
                    if($adress->getStreetNumber()!=$val){
                        $adress->setAdress($para,$val);
                    }
                    break;
                case "street" :
                    if($adress->getstreet()!=$val){
                        $adress->setAdress($para,$val);
                    }
                    break;
                case "City" :
                    if($adress->getCity()!=$val){
                        $adress->setAdress($para,$val);
                    }
                    break;
                case "Postal" :
                    if($adress->getPostalCode()!=$val){
                        $adress->setAdress($para,$val);
                    }
                    break;
                case "Country" :
                    if($adress->getCountry()!=$val){
                        $adress->setAdress($para,$val);
                    }
                    break;   
            }
            
        }

        function LoadAdress(Adress $adress){
            echo "J'habite au ".$adress->toString();
        }
            
        function GetAdress(){
            return $this->listeAdresse;
        }
    }
?>