<?php 
if(isset($_POST['forgot'])){
    $mail = $_POST['omail'];
    $valid=true;
    if(empty($mail)){
      $valid=false;
      echo "Mail non valide ou  innexistant";
    }
    if($valid){
      $db=new db_connect;
      $data=$db->select("SELECT first_name,last_name,mail FROM userr WHERE mail=?");
      $data->execute([$mail]);
      $verif_mail=$data->fetch(PDO::FETCH_ASSOC);
      if(isset($verif_mail['mail'])){
        $new_pass=rand(1000000,9999999999);
        $new_pass_hach=hash('sha1',$new_pass);
        $to=$verif_mail['mail'];
        $personne=$verif_mail['first_name'].' '.$verif_mail['last_name'];
        $header="From: Tortiplat <contact@tortiplat.com> \r\n";
        $header .= "Reply-To: ".$to."\r\n";
        $header .= "MIME-version: 1.0\r\n";
        $header .= "Content-type: text/html; charset=utf-8\n";
        $header .= "Content-Transfer-Encoding: 8bit";
        $contenu ="<html> <body> <p style='text-align: center; font-size: 18px'><b>Bonjour Mr/Mme ".$personne."</b>,</p><br/> <p style='text-align: justify'><i><b>Nouveau mot de passe : </b></i>".$new_pass."</p><br/> </body> </html>";
                          //===== Envoi du mail
                          try{
                            mail($to,"Mot de passe oublié",$contenu,$header);
                            echo "Email envoyé !";
                            $change=$db->select("UPDATE userr SET password=? WHERE mail=?");
                            $change->execute([$new_pass_hach,$verif_mail['mail']]);
                          }
                          catch(Exception $e){
                            echo 'Exception reçue : ',  $e->getMessage(), "\n";
                          }                         
      }
      else {echo "Aucun compte trouvé";}
    }
  }
?>  