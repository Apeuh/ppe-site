<?php
class User{
    private $idUser;
    private $firstname;
    private $lastname;
    private $birthdate;
    private $mail;
    private $tel;
    private $password;
    private $createAt;
    private $idAdresse;
    private $role;

    function __construct($idUser,$firstname,$lastname,$birthdate,$mail,$tel,$password,$createAt,$idAdresse,$role){
        $this->idUser=$idUser;
        $this->firstname=$firstname;
        $this->lastname=$lastname;
        $this->birthdate=$birthdate;
        $this->mail=$mail;
        $this->tel=$tel;
        $this->password=$password;
        $this->createAt=$createAt;
        $this->idAdresse=$idAdresse;
        $this->role=$role;
    }

    function getFullName(){
        $cuisnier=$this->firstname+' '+$this->lastname;
        return $cuisinier;
    }
    function getId(){
        return $this->idUser;
    }

    function getAdresse(){
        return $this->idAdresse;
    }

    function getRole(){
        return $this->role;
    }

    function getFirstName(){
        return $this->firstname;
    }

    function getLastName(){
        return $this->lastname;
    }

    function getBirthdate(){
        return $this->birthdate;
    }

    function getBirthday(){
        $date1 = date_create($this->birthdate);
        setlocale(LC_TIME, "fr_FR", "French");
        date_default_timezone_set('Europe/Paris');
        $strdate= utf8_encode(strftime("%A %d %B %G", strtotime($this->birthdate)));
        $explo = explode(" ", $strdate);
        $upper=[];
        foreach ($explo as $splitdate){
            array_push($upper,ucfirst($splitdate));
        }
        return implode(" ",$upper);
    }

    function getMail(){
        return $this->mail;
    }
    
    function getMdp(){
        return $this->password;
    }

    function getTel(){
        return $this->tel;
    }

    function setUser($para,$val){
        $dbcon=new db_connect;
        switch($para){
            case "prenom" :
                $this->firstname=$val;
                $dbcon->setVal("userr","first_name",$val,"id_user",$_SESSION['user']->getId());
                break;
            case "nom" :
                $this->lastname=$val;
                $dbcon->setVal("userr","last_name",$val,"id_user",$_SESSION['user']->getId());
                break;
            case "tel" :
                $this->tel=$val;
                $dbcon->setVal("userr","phone",$val,"id_user",$_SESSION['user']->getId());
                break;
            case "mail" :
                $this->mail=$val;
                $dbcon->setVal("userr","mail",$val,"id_user",$_SESSION['user']->getId());
                break;
            case "birthdate" :
                $this->birthdate=$val;
                $dbcon->setVal("userr","birthday",$val,"id_user",$_SESSION['user']->getId());
                break;
            case "mdp" :
                $this->mdp=$val;
                $dbcon->setVal("userr","password",$val,"id_user",$_SESSION['user']->getId());
                break;

        }
    }
}
?>