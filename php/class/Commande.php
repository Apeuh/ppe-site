<?php
class Commande{
    private $idCommande;
    private $TotalPrice;
    private $CreateAt;
    private $idClient;
    private $ListeArticle;
    private $etatCommande;

    function __construct($idCommande,$TotalPrice,$CreateAt,$idClient,$ListeArticle,$etatCommande){
        $this->idCommande=$idCommande;
        $this->TotalPrice=$TotalPrice;
        $this->CreateAt=$CreateAt;
        $this->idClient=$idClient;
        $this->ListeArticle=$ListeArticle;
        $this->etatCommande=$etatCommande;

    }

    function getDate(){
        /*$date=new DateTime($this->CreateAt);
        $date->format('d-m-Y');
        return $date;*/
        return $this->CreateAt;
    }

    function toString(){
        echo '<table border=1>
        <tr align=center><td>',$this->idClient,' || ',$this->CreateAt,'</td><td></td></tr>
        <tr align=center><td><p>', $this->ListeArticle,'</td><td>'.$this->etatCommande.'></p></td></tr>
        </table>';
    }

    public function toStringCuisine(){
        echo '<table border=1>
        <tr align=center><td>',$this->idClient,' || ',$this->CreateAt,'</td><td></td></tr>
        <tr align=center><td><p>', $this->ListeArticle,'</td><td><input type="button" value="'.$this->etatCommande.'"></p></td></tr>
        </table>';
        }
    function getClient(){
        return $this->idClient;
    }

    function getArticle(){
        return $this->ListeArticle;
    }

    function getEtat(){
        return $this->etatCommande;
    }

    function getNumero(){
        return $this->idCommande;
    }

    function getPrice(){
        return $this->TotalPrice;
    }



}

?>