<?php
class Menu{
    private $idMenu;
    private $price;
    private $nameMenu;
    private $listeArticle;

    function __construct($idMenu,$price,$nameMenu,$listeArticle){
        $this->idMenu=$idMenu;
        $this->price=$price;
        $this->nameMenu=$nameMenu;
        $this->listeArticle=$listeArticle;
    }

    
    function getId(){
        return $this->idMenu;
    }
    function getPrice(){
        return $this->price;
    }
    function getName(){
        return $this->nameMenu;
    }
    function getListeArt(){
        return $this->listeArticle;
    }
}


?>