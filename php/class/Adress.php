<?php
    class Adress {
        private $idAdress;
        private $street;
        private $streetNumber;
        private $city;
        private $postalcode;
        private $country;

        function __construct($idAdress,$street,$streetNumber,$city,$postalcode,$country){
            $this->idAdress=$idAdress;
            $this->street=$street;
            $this->streetNumber=$streetNumber;
            $this->city=$city;
            $this->postalcode=$postalcode;
            $this->country=$country;
        }

        function getStreet(){
            return $this->street;
        }

        function get_idAdress(){
            return $this->idAdress;
        }

        function getStreetNumber(){
            return $this->streetNumber;
        }

        function getCity(){
            return $this->city;
        }

        function getPostalCode(){
            return $this->postalcode;
        }

        function getCountry(){
            return $this->country;
        }

        function setAdress($para,$val){
            $dbcon=new db_connect;
            switch($para) {
                case "NStreet" :
                    $this->streetNumber=$val;
                    $dbcon->setVal("address","num_street",$val,"id_address",$_SESSION['adresse']->get_idAdress());
                    break;
                case "street" :
                    $this->street=$val;
                    $dbcon->setVal("address","street",$val,"id_address",$_SESSION['adresse']->get_idAdress());
                    break;
                case "City" :
                    $this->city=$val;
                    $dbcon->setVal("address","city",$val,"id_address",$_SESSION['adresse']->get_idAdress());
                    break;
                case "Postal" :
                    $this->postalcode=$val;
                    $dbcon->setVal("address","postal_code",$val,"id_address",$_SESSION['adresse']->get_idAdress());
                    break;
                case "Country" :
                    $this->country=$val;
                    $dbcon->setVal("address","country",$val,"id_address",$_SESSION['adresse']->get_idAdress());
                    break;  
            }
        }

        function toString(){
            echo $this->streetNumber.' '.$this->street.' '.$this->postalcode.' '.$this->city.' '.$this->country.'<br>';
        }

        function getObject(){
            return $this;
        }
        
        function getAdrresse(){
            return $this->streetNumber.' '.$this->street.' '.$this->postalcode.' '.$this->city.' '.$this->country;
        }
    }
?>