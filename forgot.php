<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Reset Password</title>
  <link rel="stylesheet" href="./css/login.css">
  <link rel="stylesheet" href="./css/style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <?php require_once('php/db_connect.php');?>
</head>
<?php 
include_once('header.php');
  ?>
<!--Login-->
<div id=mainForm>
    <div id=loginbox>
        <h3> Récuperer votre Mot de passe </h3>
        <p>Vous avez oublié votre mot de passe? <br> Pas de soucis, rentrez votre Email ici !</p>
        <form id='logform' method="post">
            <label for="oublie">E-mail :</label>
            <input id="oublie" name="omail" type=text required placeholder="E-mail"><br>
            <input id='forgot' name="forgot" type=submit value="Envoyer" >

<?php
include_once("php/fonctions/mail.php");
echo "</div>";
include_once('footer.php');
?>