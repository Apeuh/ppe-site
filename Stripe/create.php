<?php
session_start();
require '../vendor/autoload.php';

// This is your test secret API key.
\Stripe\Stripe::setApiKey('sk_test_51KMXW8It0aOvSwclNrMTNxUuPMtjXKCc8FUuwC5gYfE87mu4GMw7tj6IgU2pnO5CmmnJ80WbsC0ATlaMIxF6XqXl00UaFQ06Qu');

function calculateOrderAmount(): int {
    return 1400;
}

header('Content-Type: application/json');

try {

    // Create a PaymentIntent with amount and currency
    $paymentIntent = \Stripe\PaymentIntent::create([
        'amount' => calculateOrderAmount(),
        'currency' => 'eur',
        'automatic_payment_methods' => [
            'enabled' => true,
        ],
    ]);

    $output = [
        'clientSecret' => $paymentIntent->client_secret,
    ];

    echo json_encode($output);
} catch (Error $e) {
    http_response_code(500);
    echo json_encode(['error' => $e->getMessage()]);
}