<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Tortiplat / Menu</title>
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="icon" type="image/png" sizes="16x16" href="./assets/logo.png">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">  
  <link rel="stylesheet" href="./css/style.css">
  <link rel="stylesheet" href="./css/promo.css">
  <?php require_once('./php/db_connect.php');
  ?>
</head> 
<?php include_once('header.php');
  ?>
<div>
  <h1><span> Des promotions arriveront sous peu ! </span><br><span>Restez à l'affut !<span></h1>
</div>
<?php include_once('footer.php'); ?>